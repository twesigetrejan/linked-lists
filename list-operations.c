#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
    struct Node *head = NULL;
    int choice, data, key, value;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Insert\n");
        printf("6. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            printList(head);
            break;
        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            break;
        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            break;
        case 4:
            printf("Choose deletion:\n");
            printf("1. Delete by key\n");
            printf("2. Delete by value\n");
            printf("Enter your choice: ");
            scanf("%d", &choice);

            switch (choice)
            {
            case 1:
                printf("Enter key: ");
                scanf("%d", &key);
                deleteByKey(&head, key);
                break;
            case 2:
                printf("Enter value: ");
                scanf("%d", &value);
                deleteByValue(&head, value);
                break;
            default:
                printf("Invalid choice. Please try again.\n");
            }
            break;
        case 5:
            printf("Choose insertion:\n");
            printf("1. Insert after key\n");
            printf("2. Insert after value\n");
            printf("Enter your choice: ");
            scanf("%d", &choice);

            switch (choice)
            {
            case 1:
                printf("Enter key: ");
                scanf("%d", &key);
                printf("Enter data to insert: ");
                scanf("%d", &data);
                insertAfterKey(&head, key, data);
                break;
            case 2:
                printf("Enter search value: ");
                scanf("%d", &value);
                printf("Enter data to insert: ");
                scanf("%d", &data);
                insertAfterValue(&head, value, data);
                break;
            default:
                printf("Invalid choice. Please try again.\n");
            }
            break;
        case 6:
            exit(0);
        default:
            printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}

struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {
        printf("Memory allocation failed!\n");
        return NULL;
    }
    newNode->number = num;
    newNode->next = NULL;

    return newNode;
}

void printList(struct Node *head)
{
    while (head != NULL)
    {
        printf("%d ", head->number);
        head = head->next;
    }
    printf("\n");
}

// Implementation for appending a new node to the end of the list
void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL)
    {
        current = current->next;
    }
    current->next = newNode;
}

// Implementation for prepending a new node to the beginning of the list
void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Implementation for deleting a node based on its key value
void deleteByKey(struct Node **head, int index)
{
    if (*head == NULL)
    {
        printf("List is empty!\n");
        return;
    }
    if (index == 0)
    {
        struct Node *temp = *head;
        *head = (*head)->next;
        free(temp);
        return;
    }
    int count = 0;
    struct Node *current = *head;
    struct Node *previous = NULL;
    while (current != NULL && count < index)
    {
        previous = current;
        current = current->next;
        count++;
    }
    if (current == NULL)
    {
        printf("Invalid index!\n");
        return;
    }
    if (previous != NULL)
    {
        previous->next = current->next;
    }
    else
    {
        *head = current->next;
    }
    free(current);
    printf("Value deleted at index %d\n", index);
}

// Implementation for deleting a node based on its data value
void deleteByValue(struct Node **head, int value)
{
    if (*head == NULL)
    {
        printf("List is empty!\n");
        return;
    }
    if ((*head)->number == value)
    {
        struct Node *temp = *head;
        *head = (*head)->next;
        free(temp);
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL && current->next->number != value)
    {
        current = current->next;
    }
    if (current->next == NULL)
    {
        printf("Value not found!\n");
        return;
    }
    struct Node *temp = current->next;
    current->next = current->next->next;
    free(temp);
}

// Implementation for inserting a new node after a node with a specific key
void insertAfterKey(struct Node **head, int index, int value)
{
    if (index < 0)
    {
        printf("Invalid index! Index cannot be negative.\n");
        return;
    }
    int count = 0;
    struct Node *current = *head;
    struct Node *previous = NULL;
    while (current != NULL && count < index)
    {
        previous = current;
        current = current->next;
        count++;
    }
    if (current == NULL)
    {
        printf("Invalid index! Index out of bounds.\n");
        return;
    }
    struct Node *newNode = createNode(value);
    newNode->next = current->next;
    current->next = newNode;
    printf("Value inserted after index %d\n", index);
}

// implementation for inserting for inserting a node after a specific value
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *current = *head;
    while (current != NULL && current->number != searchValue)
    {
        current = current->next;
    }

    if (current == NULL)
    {
        printf("Value not found!\n");
        return;
    }

    struct Node *newNode = createNode(newValue);
    newNode->next = current->next;
    current->next = newNode;
}

// Questions:
// Implement the prototypes defined above.
// Reimplement the main method using a switch and complete the pending steps.
